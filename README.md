# Overview

This repository sets up ubuntu on my desktop a clean install, and can also be run to update my settings/packages on a regular basis.

# Pre-installation

To download the repository, run

`sh -c "$(wget -qO- https://bitbucket.org/crottman/setup_ubuntu/raw/master/pre-install)"`

# Main script

The main script can be run by

`~/git/setup-ubuntu/setup-ubuntu`

# Details

The setup-ubuntu does the following (in order):

* Sets up passwordless sudo
* Adds apt repositories (see `bin/add-apt-repos` to edit repo list
* Installs apt packages (see `requirements-apt.txt`)
* Installs python packages (see `requirements.txt`)
* Installs custom packages (oh-my-zsh, Google Chrome, filebot)
* Sets up ssh (and gives keys to add to bitbucket/gitlab)
* Downloads and sets up config files
* Sets up emacs config
* Sets default shell to zsh
* Deletes Pictures/Public/Templates/Videos/Musics folders in home directory
* Sets up crontab (for beryl server backups)
* Sets up hard drives (for beryl server)
